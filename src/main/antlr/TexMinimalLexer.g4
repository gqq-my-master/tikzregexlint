lexer grammar TexMinimalLexer;

LITERAL_PERCENT: '\\%';

COMMENT: '%' .*? NEWLINE -> channel(2);

NEWLINE:'\r'? '\n' ;

WS : [ \t]+; // -> skip ;

UNKNOWN: .;