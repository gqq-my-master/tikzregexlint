import java.io.*;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.regex.Pattern;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CharStreams;
import org.antlr.v4.runtime.CommonTokenStream;

public class Program {
	public static void main(String[] args) throws IOException {
		FileInputStream fstream = new FileInputStream("B:\\a.txt");
		BufferedReader br = new BufferedReader(new InputStreamReader(fstream));

		StringBuilder sb = new StringBuilder();
		String strLine;


		while ((strLine = br.readLine()) != null) {
			strLine = strLine.stripLeading();
			TexMinimalLexer lexer = new TexMinimalLexer(CharStreams.fromString(strLine + "\r\n"));

			CommonTokenStream tokens = new CommonTokenStream(lexer);
			tokens.fill();

			int p = strLine.length();
			var tokens2 = tokens.getTokens();
			for (var t : tokens2) {
				if (t.getChannel() == 2) //comment
				{
					p = t.getCharPositionInLine();
					break;
				}
			}
			sb.append(strLine.substring(0, p).stripLeading() + "\r\n");
		}

		String content = sb.toString();

		System.out.println("Valid content is:");
		System.out.println(content);

		checkDrawOptions(content);
		checkCycle(content);
		checkDraw(content);
		checkKeyHandlers(content);
	}

	static void checkDrawOptions(String content) {
		var p = Pattern.compile("\\\\draw\\[.*\\bdraw\\b.*?\\]", Pattern.DOTALL);
		var m = p.matcher(content);
		if (m.find()) {
			System.out.println("Unnecessary option 'draw' in command \\draw");
			System.out.println(m.group(0));
		}
	}

	static void checkCycle(String content) {
		var p = Pattern.compile("\\\\draw(((?!--).)*)--(((?!--).)*)--cycle", Pattern.DOTALL);
		var m = p.matcher(content);
		while (m.find()) {
			System.out.println("Closing path " + m.group(1) + " -- " + m.group(3) + " has no visual effect.");
		}
	}

	static void checkDraw(String content) {
		var p = Pattern.compile("arc\\[[^]]*\\bdraw\\b.*\\]", Pattern.DOTALL);
		var m = p.matcher(content);
		while (m.find()) {
			System.out.println("The draw option has no effect in the arc operation. Use command \\draw or \\path[draw] at the beginning.");
			System.out.println(m.group(0));
		}
	}

	static void checkKeyHandlers(String content) {
		var p = Pattern.compile("[,\\[][\\s\\r\\n]*(\\w[\\w\\s/]+)/\\.style[\\s\\r\\n]*\\=[\\s\\r\\n]*\\{(.*?)\\}", Pattern.DOTALL);
		var m = p.matcher(content);
		while (m.find()) {
			String key = m.group(1);
			String value = m.group(2);

			if (value.contains("#1") == false) {
				//Check if key=xxx is used.
				var p2 = Pattern.compile("\\b" + Pattern.quote(key) + "[\\s\\r\\n]*=[\\s\\r\\n]*(\\w+|\\{(.*?)\\})");
				var m2 = p2.matcher(content);
				if (m2.find()) {
					System.out.printf("Found `%1$s`.\nThe key '%2$s' is defined without parameters, but it's used with argument '%3$s'.\n", m2.group(0), key, m2.group(1));
					System.out.printf("If you're redefining '%1$s', you have to write `%1$s/.style=%2$s` or `%1$s/.append style=%2$s`.\n", key, m2.group(1));
				}
			}
		}
	}
}
